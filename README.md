# README #
## Introducción ##
Este proyecto, se desarrolló con el fin de realizar una prueba técnica para la incorporación de **almundo.com**.
El cual está dividido en dos partes, el frontEnd dentro de la capa **app** y el backEnd en la capa **server**.
Aúnque, existe un repositorio diferente en el que solo se encuentra desarrollado el backEnd:

    https://bitbucket.org/crisdanilo/almundo.com

Sin embargo, se adaptó para desarrollar una aplicación universal dentro de este repositorio.

### Clonar el proyecto e instalar dependencias ###
Lo primer que debemos hacer es bajarnos el proyecto a nuestro entorno local

    git clone https://crisdanilo@bitbucket.org/crisdanilo/almundo.git
    
Luego debemos instalar todas las dependencias necesarias para el proyecto.
    
    npm install
    
El proyecto se ejecuta con:

    npm run start
    
### Configuración de base de datos ###
El proyecto tiene un archivo de configuración llamado **default.json**,
dentro de la carpeta **server/config**. El motor de base de datos utilizado es **MySQL**.

    "dbConfig": {
        "host": "localhost",
        "user": "root",
        "password": "root",
        "port": 3306,
        "database": "almundo"
    }

### Creación de tabla ###

El script SQL para la creación de la tabla **hotels**, es el siguiente:

    CREATE  TABLE `almundo`.`hotels` (
      `id` INT NOT NULL AUTO_INCREMENT ,
      `name` VARCHAR(255) NOT NULL ,
      `stars` INT(1) NOT NULL DEFAULT 0 ,
      `price` FLOAT NOT NULL ,
      PRIMARY KEY (`id`) ,
      UNIQUE INDEX `id_UNIQUE` (`id` ASC) );

Y el insert para agregar algunos datos de prueba

    INSERT INTO `almundo`.`hotels` (`name`, `stars`, `price`) VALUES ('Hotel Emperador', '3', '1596');
    INSERT INTO `almundo`.`hotels` (`name`, `stars`, `price`) VALUES ('Petit Palace San Bernardo', '4', '2145');
    INSERT INTO `almundo`.`hotels` (`name`, `stars`, `price`) VALUES ('Hotel Nuevo Boston', '2', '861');

### Endpoints ###

El proyecto tiene dos endpoints que realizan la consulta a base de datos para obtener los datos de la tabla **hotels**, en el puerto **3000**:

    GET http://localhost:3000/api/v1/hotels
    GET http://localhost:3000/api/v1/hotels/:id
    
El endpoint de **hotels** puede recibir un valor más por querystring en el que se puede ordenar los resultados. Ej:

    http://localhost:3000/api/v1/hotels/?orderBy=name
    http://localhost:3000/api/v1/hotels/?orderBy=name desc
    http://localhost:3000/api/v1/hotels/?orderBy=name, stars desc

Las rutas se encuentran en el archivo **Routes.js** dentro de la carpeta **server/app**.
    