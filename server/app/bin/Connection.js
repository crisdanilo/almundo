import Config from 'config';
import mysql from 'mysql2';

let instance = null;

export default class Connection
{
    constructor() {
        if (!instance) {
            instance = this;
        }
        return instance;
    }

    start() {
        const connection = mysql.createConnection(this.dbConfig);
        connection.connect(error => {
            if (error) {
                throw new TypeError(`error connecting: ${error.stack}`);
            }
            console.log(`connected as id ${connection.threadId}`);
        });

        return connection;
    }

    execute(query, callback) {
        const connection = this.start();
        connection.query(query, null, (error, results) => {
            if (error) {
                callback(error, null);
                throw new TypeError(`Can not get results: ${error}`);
            }
            callback(null, results);
        });
        connection.end();
    }
}

Connection.prototype.dbConfig = Config.get('dbConfig');
