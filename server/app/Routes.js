'use strict';
import ApiController from '../controllers/ApiController';
import path from 'path';
const api = new ApiController;

export const Routes = (router, app) => {
    app.get('*', (req, res, next) => {
        const isStrApi = req.originalUrl.indexOf('api/v1');
        if (isStrApi < 0 && req.accepts('html')){
            res.sendFile(path.join(__dirname, '../public', 'index.html'));
        } else {
            next();
        }
    });

    app.use('/api/v1', router);
    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    router.get('/hotels', api.getAll);
    router.get('/hotels/:id', api.getOne);
};
