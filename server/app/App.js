import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import url from 'url';
import path from 'path';
import { Routes } from './Routes';

let instance = null;

export default class App
{
    constructor() {
        if (!instance) {
            instance = this;
        }
        return instance;
    }

    start() {
        this._configure();
        this.app.listen(3000, () => {
            console.log('Node server running on http://localhost:3000');
        });
    }

    render() {
        const router = express.Router();
        Routes(router, this.app);
    }

    _configure() {
        this.app = express();
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(express.static(path.join(__dirname, '../public')));
        this.app.use(helmet());
        this.app.disable('x-powered-by');
        this.app.all('*', (req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header('Access-Control-Allow-Methods', 'HEAD, GET, PUT, POST, DELETE, OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-type, Accept');
            this.handle(req, res, next);
        })
    }

    handle(req, res, next) {
        let path = url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.originalUrl,
        });
        this.render();
        console.log(`${req.method} ${res.statusCode} ${path}`);
        if (req.method === 'OPTIONS') {
            res.status(200).end();
        } else {
            next();
        }
    }
}

App.prototype.app = {};
