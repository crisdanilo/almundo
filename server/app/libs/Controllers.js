export default class Controllers
{
    constructor() {
        if (this.constructor === Controllers) {
            throw new TypeError('Can not construct abstract class.');
        }
    }

    response(object, res, id = null, orderBy = null) {
        let result = {
            'status': 'ok'
        };
        object['getJSON'].call(object, id, orderBy, (error, response) => {
            if (!error) {
                let obj;
                if (response.length > 0){
                    obj = {
                        'data': response,
                        'status_code': res.statusCode
                    };
                } else {
                    res.status(404);
                    obj = {
                        'status_code': 404,
                        'message': 'NOT FOUND'
                    };
                    result.status = 'error';
                }
                result = Object.assign({}, obj, result);
                res.json(result);
            }
        });
    }
}
