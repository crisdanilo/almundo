import Model from '../app/libs/Models';

export default class Hotels extends Model
{

}

Hotels.prototype.table = 'hotels';
Hotels.prototype.fields = ['name', 'stars', 'price'];
