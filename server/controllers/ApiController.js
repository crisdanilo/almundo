import Controllers from '../app/libs/Controllers';
import Hotels from '../models/Hotels';

export default class ApiController extends Controllers
{
    constructor() {
        super();
    }

    getAll(req, res) {
        let orderBy = null;
        if (req.query.orderBy) {
            orderBy = req.query.orderBy;
        }
        super.response(new Hotels, res, null, orderBy);
    }

    getOne(req, res) {
        const id = req.params.id;
        super.response(new Hotels, res, id);
    }
}
