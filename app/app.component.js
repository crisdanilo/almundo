export const AppComponent = {
    template: `
        <app-header></app-header>
        <div class="container" ui-view></div>
        <app-footer></app-footer>
      `
};
