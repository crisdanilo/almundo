import angular from 'angular'
import hotel from './hotel'

const components = angular
    .module('app.components', [hotel])
    .name;

export default components;
