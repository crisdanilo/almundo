class HotelService
{
    constructor($http) {
        this.$http = $http;
    }

    all() {
        return this.$http.get('/api/v1/hotels').then(response => response.data.data);
    }
}

HotelService.$inject = ['$http'];

export default HotelService;
