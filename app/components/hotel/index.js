import angular from 'angular';
import { HotelListComponent } from './hotel-list/hotel-list.component';
import { HotelItemComponent } from './hotel-item/hotel-item.component';
import StarFilter from './hotel-item/stars.filter';
import HotelService from '../services/hotel.service';

const hotel = angular
    .module('hotel', [])
    .service('HotelService', HotelService)
    .component('hotelList', HotelListComponent)
    .component('hotelItem', HotelItemComponent)
    .filter('stars', StarFilter)
    .config(($stateProvider, $urlRouterProvider) => {
        $stateProvider
            .state('hotels', {
                url: '/',
                component: 'hotelList',
                resolve: {
                    hotels: function (HotelService) {
                        return HotelService.all()
                    }
                }
            });
        $urlRouterProvider.otherwise('/')
    })
    .name;

export default hotel;
