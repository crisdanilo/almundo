export const HotelListComponent = {
    bindings: {
        hotels: '<'
    },
    template: `
        <ul>
            <hotel-item ng-repeat="hotel in $ctrl.hotels" data="hotel">
            </hotel-item>
        </ul>
    `
};
