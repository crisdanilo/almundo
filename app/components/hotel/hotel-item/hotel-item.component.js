export const HotelItemComponent = {
    bindings: {
        data: '<'
    },
    template: `
        <li>
            <div class="col s12">
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="http://lorempixel.com/300/190/city/6">
                        <span class="card-top blue darken-4">¡Hotel Recomendado!</span>
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <h6 class="card-title">{{$ctrl.data.name}}</h6>
                            <div ng-bind-html='$ctrl.data.stars | stars'></div>
                            <span class="orange-text"><i class="material-icons md-18 md-orange">flag</i> Solo la habitación</span>
                            <p>
                                <i class="material-icons md-18">local_bar</i>
                                <i class="material-icons md-18">beach_access</i>
                                <i class="material-icons md-18">photo_filter</i>
                                <i class="material-icons md-18">local_cafe</i>
                                <i class="material-icons md-18">accessibility</i>
                                <i class="material-icons md-18">spa</i>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    `
};
