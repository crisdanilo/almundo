const StarFilter = ($sce) => {
    return (number) => {
        let star = '';
        for (let i = 0; i < number; i++) {
            star += '<i class="material-icons md-18 md-yellow">star_rate</i>';
        }
        return $sce.trustAsHtml(star);
    }
};

StarFilter.$inject = ['$sce'];

export default StarFilter;
