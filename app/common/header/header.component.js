export const HeaderComponent = {
    template: `
        <nav>
            <div class="nav-wrapper indigo">
                <a href="#" class="brand-logo">Logo</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="badges.html">Ingresar</a></li>
                    <li><a href="collapsible.html">Sucursales</a></li>
                    <li>
                        <a class="dropdown-button" href="#!" >
                            Llamanos<i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>
                    <li><a href="collapsible.html">Ayuda</a></li>
                </ul>
            </div>
        </nav>
    `
};
