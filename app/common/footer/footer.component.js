export const FooterComponent = {
    template: `
        <footer class="page-footer blue-grey darken-2">
            <div class="footer-copyright blue-grey darken-1">
                <div class="container">
                    © 2017 Cristian Rivas.
                </div>
            </div>
        </footer>
    `
};
