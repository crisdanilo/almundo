import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import { common } from './common';
import components from './components';
import { AppComponent } from './app.component';

const root = angular
    .module('alMundo', [
        uiRouter,
        common,
        components
    ])
    .component('app', AppComponent);

document.addEventListener('DOMContentLoaded', () => angular.bootstrap(document, ['alMundo']));

export default root;